const mongoose = require('mongoose');
const dbURI = 'mongodb+srv://zhanlun:mango1234@firstcluster-qpozz.gcp.mongodb.net/test_staff';
mongoose.connect(dbURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useMongoClient: true,
    dbName: 'test_staff' 
});
mongoose.connection.on('connected', () => {
 console.log(`Mongoose connected to ${dbURI}`);
});

require('./staff');
require('./user');
require('./user_details');