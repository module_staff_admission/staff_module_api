const mongoose = require('mongoose');
const Nation = mongoose.model('Nation');
const LocalState = mongoose.model('LocalState');
const Gender = mongoose.model('Gender');
const Religion = mongoose.model('Religion');
const Race = mongoose.model('Race');
const MarriageStatus = mongoose.model('MarriageStatus');
const EmployeeStatus = mongoose.model('EmployeeStatus');

const nationList = (req, res) => {
    Nation.find({}, (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": data});
        }

        return res
            .status(200)
            .json(data);
    });
}

const localStateList = (req, res) => {
    LocalState.find({}, (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": data});
        }

        return res
            .status(200)
            .json(data);
    });
}

const genderList = (req, res) => {
    Gender.find({}, (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": data});
        }

        return res
            .status(200)
            .json(data);
    });
}

const religionList = (req, res) => {
    Religion.find({}, (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": data});
        }

        return res
            .status(200)
            .json(data);
    });
}

const raceList = (req, res) => {
    Race.find({}, (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": data});
        }

        return res
            .status(200)
            .json(data);
    });
}

const marriageStatusList = (req, res) => {
    MarriageStatus.find({}, (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": data});
        }

        return res
            .status(200)
            .json(data);
    });
}

const employeeStatusList = (req, res) => {
    EmployeeStatus.find({}, (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": data});
        }

        return res
            .status(200)
            .json(data);
    });
}


module.exports = {
    nationList,
    localStateList,
    genderList,
    religionList,
    raceList,
    marriageStatusList,
    employeeStatusList
}